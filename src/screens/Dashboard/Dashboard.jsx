import React from "react";
import base64 from "react-native-base64";
import { BLUELOADER } from "utils/constants";

const Dashboard = (
  // { navigatePage, dashboardData }
  ) => {
  return (
    <div className="page-body">
      <div className="container-fluid">
        <div className="page-header">
          <div className="row">
            <div className="col">
              <div className="page-header-left">
                <h3>Dashboard</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Container-fluid starts*/}
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-md-6">
                <div
                  className="card box-hover"
                  // onClick={() => navigatePage(`/business-list/${base64.encode('1')}`)}
                  style={{ cursor: "pointer" }}
                >
                  <div className="card-body">
                    <div className="chart-widget-dashboard">
                      <div className="media">
                        <div className="media-body" style={{marginTop:'15px', marginBottom:'15px'}}>
                          <h5 className="mt-0 mb-0 f-w-600 text-center">
                            <i data-feather="dollar-sign" />
                            <span className="counter">Users</span>
                          </h5>
                          <h5 className="dashboard-count text-center">
                            1
                          </h5>
                        </div>
                        <i data-feather="tag" />
                      </div>
                      <div className="dashboard-chart-container">
                        <div className="small-chart-gradient-1" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div
                  className="card box-hover"
                  // onClick={() => navigatePage(`/user-list/${base64.encode('1')}`)}
                  style={{ cursor: "pointer" }}
                >
                  <div className="card-body">
                    <div className="chart-widget-dashboard">
                      <div className="media">
                        <div className="media-body" style={{marginTop:'15px', marginBottom:'15px'}}>
                          <h5 className="mt-0 mb-0 f-w-600 text-center">
                            <i data-feather="dollar-sign" />
                            <span className="counter">Customers</span>
                          </h5>
                          <h5 className="dashboard-count text-center">
                            35
                          </h5>
                        </div>
                        <i data-feather="shopping-cart" />
                      </div>
                      <div className="dashboard-chart-container">
                        <div className="small-chart-gradient-2" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
