import Breadcrumb from 'component/common/breadcrumb'
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { Link, json, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import NoDataFound from "component/NoDataFound";
import base64 from "react-native-base64";
import { YELLOWLOADER } from "utils/constants";
import { getCustomerList } from "reduxStore/action/customerAction";
import Select from 'react-select';
import { RxCross1 } from "react-icons/rx";
import "../component/Role.css"
import { getMasterList } from 'reduxStore/action/masterAction';
import { FETCH_MASTER_LIST } from 'reduxStore/types';
import Pagination from 'component/common/Pagination';
const Role = (props) => {
    const {
        options,
        search,
        setSearch,
        handleSubmit,
        handleSearch,
        listData,
        statusOption,
        currentPost,
        postPerPage,
        handleSetCurrentPage,
        handlePrevious,
        handleNext,
        totalPages,
        currentPage,
        editItem,
        setEditItem,
        handleEditIconClick
    } = props;
    const { type, title, status } = search;


    return (
        <div className="page-body">
            <div className="modal fade" id="addnewrole" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2 className="modal-title " id="exampleModalLabel">Create Master</h2>
                            <div data-bs-dismiss="modal" aria-label="Close"><RxCross1 /></div>
                            {/* <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> */}
                        </div>
                        <div className="modal-body">
                            <form id="myForm" onsubmit="submitForm()">
                                <div className="">


                                    <div className="d-flex">
                                        <div className="cardText ">
                                            Type
                                        </div>
                                        <div className='col-md-11'>
                                            <Select
                                                id='dropdown'
                                                placeholder='type...'
                                                value={options.label}
                                                onChange={(selectOption) =>
                                                    setSearch({
                                                        ...search,
                                                        type: selectOption.value,
                                                    })
                                                }
                                                options={options}
                                            />
                                        </div>
                                    </div>

                                </div>
                                <div className=" d-flex mt-3">
                                    <div className="cardText">Title </div>&nbsp;
                                    <div
                                        className="input-group date col-md-11 "
                                        id="dt-date"
                                        data-target-input="nearest"
                                    >
                                        <input
                                            className="form-control "
                                            id="inputField"
                                            type="text"
                                            style={{ textTransform: "capitalize" }}
                                            autoComplete="off"
                                            value={title}
                                            onChange={(e) =>
                                                setSearch({
                                                    ...search,
                                                    title: e.target.value,
                                                })
                                            }
                                            placeholder="title..."

                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                            <button type="button" className="btn btn-grey btn-accept" onClick={handleSubmit} data-bs-dismiss="modal" aria-label="Close">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="editmaster" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2 className="modal-title edit-master" id="exampleModalLabel">Edit Master</h2>
                            <div data-bs-dismiss="modal" aria-label="Close"><RxCross1 /></div>
                            {/* <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> */}
                        </div>
                        <div className="modal-body">
                            <form id="myForm" onsubmit="submitForm()">
                                <div className="">


                                    <div className="d-flex">
                                        <div className="cardText ">
                                            Type
                                        </div>
                                        <div className='col-md-11'>
                                            <Select
                                                id='dropdown'
                                                placeholder='type...'
                                                value={editItem.type ? { label: editItem.type, value: editItem.type } : null}
                                                onChange={(selectedOption) => setEditItem({ ...editItem, type: selectedOption.value })}
                                                options={options}
                                            />
                                        </div>
                                    </div>

                                </div>
                                <div className=" d-flex mt-3">
                                    <div className="cardText">Title </div>&nbsp;
                                    <div
                                        className="input-group date col-md-11 "
                                        id="dt-date"
                                        data-target-input="nearest"
                                    >
                                        <input
                                            className="form-control "
                                            id="inputField"
                                            type="text"
                                            style={{ textTransform: "capitalize" }}
                                            autoComplete="off"
                                            value={editItem.title}
                                            onChange={(e) => setEditItem({ ...editItem, title: e.target.value })
                                            }
                                            placeholder="title..."

                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                            <button type="button" className="btn btn-grey btn-accept" >Edit</button>
                        </div>
                    </div>
                </div>
            </div>
            <Breadcrumb
                title="Role"
                parent="Manage Role"
                titleactive="Role List"
            />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12 col-xl-12">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="card">
                                    <div className="card-body">
                                        <form className="theme-form" >
                                            <div className="row">

                                            </div>
                                            <div className="row pt-3">
                                                <div className="col-md-3">
                                                    <span className="cardText ">
                                                        Type
                                                    </span>

                                                    <div className="">
                                                        <Select
                                                            placeholder='type...'
                                                            // value={''}
                                                            // onChange={''}
                                                            // options={options}
                                                            value={options.label}
                                                            onChange={(selectOption) =>
                                                                setSearch({
                                                                    ...search,
                                                                    type: selectOption.value,
                                                                })
                                                            }
                                                            options={options}
                                                        />
                                                    </div>

                                                </div>
                                                <div className="col-md-3">
                                                    <span className="cardText">Title</span>
                                                    <div
                                                        className="input-group date"
                                                        id="dt-date"
                                                        data-target-input="nearest"
                                                    >
                                                        <input
                                                            className="form-control "
                                                            type="text"
                                                            style={{ textTransform: "capitalize" }}
                                                            autoComplete="off"
                                                            name="search_title"
                                                            value={search.title}
                                                            onChange={(e) =>
                                                                setSearch({
                                                                    ...search,
                                                                    title: e.target.value
                                                                })
                                                            }

                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-md-3">
                                                    <span className="cardText ">
                                                        Status
                                                    </span>

                                                    <div className="">
                                                        <Select
                                                            placeholder='status...'
                                                            value={statusOption.label}
                                                            onChange={(selectOption) =>
                                                                setSearch({
                                                                    ...search,
                                                                    status: selectOption.value,
                                                                })
                                                            }
                                                            options={statusOption}
                                                        />
                                                    </div>

                                                </div>
                                                <div className="col-md-3 pt-4">
                                                    <div className="form-group d-flex justify-content-end">
                                                        <button
                                                            className="btn btn-primary mr-4"
                                                            type="button"
                                                            // onClick={(e) => {handleSearch(e)}}
                                                            onClick={handleSearch}
                                                        >
                                                            Search
                                                        </button>
                                                        <button
                                                            className="btn btn-dark"
                                                            type="sumbit"
                                                        // onClick={() =>
                                                        //   setForm({
                                                        //     start_date: null,
                                                        //     end_date: null,
                                                        //     search_business: null,
                                                        //     search_location: null,
                                                        //   })
                                                        // }
                                                        // onClick={handleResetInput}

                                                        >
                                                            Reset
                                                        </button>

                                                    </div>
                                                </div>

                                            </div>

                                            <div className="d-flex justify-content-end ">
                                                <Link
                                                    className="btn btn-primary "
                                                    data-bs-toggle="modal" data-bs-target="#addnewrole"
                                                >
                                                    &nbsp; &nbsp; Add New Role &nbsp; &nbsp;
                                                </Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div className="row">
                                <div className="col-sm-12 col-xl-12">
                                    <div className="card">

                                        <div className="table-responsive">
                                            <table className="table table-border-horizontal">
                                                <colgroup>
                                                    <col style={{ width: '25%' }} /> {/* Adjust the width values as needed */}
                                                    <col style={{ width: '25%' }} />
                                                    <col style={{ width: '25%' }} />
                                                    <col style={{ width: '25%' }} />
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th scope="col" className="text-center">
                                                            Title
                                                        </th>
                                                        <th scope="col" className="text-center">
                                                            Type
                                                        </th>
                                                        <th scope="col" className="text-center">
                                                            Status
                                                        </th>
                                                        <th scope="col" className="text-center">
                                                            Action.
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentPost && Array.isArray(currentPost) && currentPost.map((item) => (

                                                        <tr key={item.id}>
                                                            <td className="text-center" style={{ textTransform: "capitalize" }}>
                                                                {item?.title}
                                                            </td>
                                                            <td className="text-center">
                                                                {item?.type}
                                                            </td>
                                                            <td className="text-center">
                                                                {item?.status === 1 ? (
                                                                    <div>
                                                                        <span style={{ width: '10px', height: '10px', backgroundColor: 'green', borderRadius: '50%', display: 'inline-block', marginRight: '5px' }}></span>
                                                                        <span>active</span>
                                                                    </div>
                                                                ) : (
                                                                    <div>
                                                                        <span style={{ width: '10px', height: '10px', backgroundColor: 'red', borderRadius: '50%', display: 'inline-block', marginRight: '5px' }}></span>
                                                                        <span>inactive</span>
                                                                    </div>
                                                                )}
                                                                {/* {item?.status} */}
                                                            </td>
                                                            <td className="text-center">

                                                                <Link
                                                                    data-bs-toggle="modal" data-bs-target="#editmaster"
                                                                >
                                                                    <i className="fa fa-edit" aria-hidden="true" style={{ cursor: 'pointer', color: '#3346d4' }} onClick={() => handleEditIconClick(item)}></i>
                                                                </Link>

                                                            </td>
                                                        </tr>
                                                    )
                                                    )}
                                                </tbody>
                                            </table>
                                            <Pagination
                                                postPerPage={postPerPage}
                                                totalPost={listData?.length}
                                                setCurrentPage={handleSetCurrentPage}
                                                handleNext={handleNext}
                                                handlePrevious={handlePrevious}
                                                totalPages={totalPages}
                                                currentPage={currentPage}
                                            />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            {/* <ReactPaginate
                                    breakLabel="..."
                                    nextLabel="next >"
                                    // onPageChange={(e) => handlePageClick(e)}
                                    pageCount={2}
                                    previousLabel="< previous"
                                    pageClassName="page-item"
                                    pageLinkClassName="page-link"
                                    previousClassName="page-item"
                                    previousLinkClassName="page-link"
                                    nextClassName="page-item"
                                    nextLinkClassName="page-link"
                                    breakClassName="page-item"
                                    breakLinkClassName="page-link"
                                    containerClassName="pagination"
                                    activeClassName="active"
                                    // forcePage={Number(page_no)}
                                  />
                                   */}
                            {/* <Pagination
                                itemsPerPage={itemLimit}
                                totalrecord={totalrecord}
                                itemOffset={itemOffset}
                                setItemOffset={setItemOffset}
                                getdataItem={(getoffset) => getBranchList(getoffset)}
                            /> */}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Role