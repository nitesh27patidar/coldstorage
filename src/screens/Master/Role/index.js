import React, { useEffect, useState } from 'react'
import Role from './component/Role'
import { getMasterList, getNewMaster } from 'reduxStore/action/masterAction';
import { useDispatch, useSelector } from 'react-redux';
import { Pagination } from '@mui/material';

const Index = () => {

    useEffect(() => {
        // getBranchList(itemOffset);
        // getMasterList();
    }, []);
    const userList = useSelector((state) => state.getMasterList)
    const [isLoading, setisLoading] = useState(false);
    const listData = userList?.response?.data;
    const [totalrecord, setTotalrecord] = useState(0);

    const [itemOffset, setItemOffset] = useState(0);
    const [itemLimit, setItemLimit] = useState(5);
    const [branchList, setBranchlist] = useState([]);

    const [currentPage, setCurrentPage] = useState(1)
    const [postPerPage, setPostPerPage] = useState(5)
    const [localCurrentPage, setLocalCurrentPage] = useState(1);

    const [editItem, setEditItem] = useState({ id: null, title: '', type: '', status: 0 });
    
    const totalPost = listData?.length;
    const totalPages = Math.ceil(totalPost / postPerPage);
    const handleNext = () => {
        if (currentPage < totalPages) {
            setLocalCurrentPage(currentPage + 1);
            setCurrentPage(currentPage + 1);
        }
    };

    const handlePrevious = () => {
        if (currentPage > 1) {
            setLocalCurrentPage(currentPage - 1);
            setCurrentPage(currentPage - 1);
        }
    };



 
   

    const [search, setSearch] = useState({
        title: "",
        type: "",
        status: 1


    });
    const [listSearch, setlistSearch] = useState({
        title: search.title,
        type: search.type,
        status: 1,
        limit: 50,
        offset: 0,
    })

    const dispatch = useDispatch();
    useEffect(() => {

        dispatch(getMasterList(listSearch));
    }, [dispatch]);

    const options = [
        { value: 'ROLE', label: 'ROLE' },
        { value: 'FLOOR', label: 'FLOOR' },
        { value: 'BLOCK', label: 'BLOCK' }
    ];

    const statusOption = [
        { value: 1, label: 1 },
    ]

    const handleSubmit = () => {
        dispatch(getNewMaster(search))

    }

    const [form, setForm] = useState({
        search_title: "",
        claim_status: "null",
        approve_status: "null",
    });

    const handleInputField = (e) => {
        const nextFormState = {
            ...form,
            [e.target.name]: e.target.value,
        };
        setForm(nextFormState);
    };


    const handleSearch = (e) => {
        const updatedListSearch = {
            title: search.title,
            type: search.type,
            status: search.status,
            limit: 50,
            offset: 0,
        };
        dispatch(getMasterList(updatedListSearch));
    };



    const lastPostIndex = currentPage * postPerPage;
    const firstPostIndex = lastPostIndex - postPerPage;
    const currentPost = Array.isArray(listData) ? listData.slice(firstPostIndex, lastPostIndex) : [];

    const handleSetCurrentPage = (page) => {
        setCurrentPage(page);
    };
    
    const handleEditIconClick = (item) => {
        setEditItem(item);
    };

    return (
        <>
            <Role options={options}
                isLoading={isLoading}
                search={search}
                setSearch={setSearch}
                handleSubmit={handleSubmit}
                handleInputField={handleInputField}
                form={form}
                handleSearch={handleSearch}
                listData={listData}
                statusOption={statusOption}
                listSearch={listSearch}
                itemLimit={itemLimit}
                // getBranchList={(getoffset) => getBranchList(getoffset)}
                branchList={branchList}
                currentPost={currentPost}
                postPerPage={postPerPage}
                totalPost={totalPost}
                handleSetCurrentPage={handleSetCurrentPage}
                handleNext={handleNext}
                handlePrevious={handlePrevious}
                totalPages={totalPages}
                currentPage={currentPage}
                editItem={editItem}
                setEditItem={setEditItem}
                handleEditIconClick={handleEditIconClick}
            />
           
        </>
    )
}

export default Index