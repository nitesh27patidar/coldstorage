import { useParams } from "react-router-dom";
import { Fragment, useEffect, useState } from "react";

const ViewCustomer = (props) => {
  const{customerList}=props;
  
  const { id } = useParams();
  const [customerData,setCustomerData]= useState(null);
  

  useEffect(()=>{
    console.log("customerlist hh",customerList)
    const foundCustomer = customerList?.find(
        (customer) => customer.id === id
      );
      setCustomerData(foundCustomer);

  }, [customerList,id]);

  return (
    <>
      <Fragment>
        <div className="page-body">
          <div className="container-fluid">
            <div className="page-header">
              <div className="row">
                <div className="col">
                  <div className="page-header-left">
                    <h3>Business</h3>
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <a>
                          <i
                            className="fa fa-home theme-fa-icon"
                            aria-hidden="true"
                            // onClick={() => navigatpage("/dashboard")}
                          ></i>
                        </a>
                      </li>
                      <li className="breadcrumb-item">Manage Customers</li>
                      <li className="breadcrumb-item active">
                        Add New Customer
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p>Customer Details for ID: {id}</p>
          <div className="container-fluid">
            <div className="row ">
              <div className="col-sm-12">
                <div className="card">
                  <div className="card-header">
                    <h5>View All Customer Details</h5> 
                    </div>
                  <div className="im-biz-detail-container">
                    <div>
                      <div className="im-detail-container">
                        <form>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              First Name: <span>{customerData?.firstname}</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Last Name: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Email: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Mobile No: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Address: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Items: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Quantity: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Price: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </Fragment>
    </>
  );
};

export default ViewCustomer;
