import React, { Fragment,  useState } from "react";
import apiEndPoints from "utils/apiEndPoints";
import { apiCall } from "utils/httpClient";
import AddCustomer from "./AddCustomer";
import { toast } from "react-toastify";
import AllCustomerList from "./AllCustomerList";

const Index = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const [customerDetails, setCustomerDetails] = useState({});
  const [formError, setFormError] = useState({});
  const [showValiErrorMsg, setShowValiErrorMsg] = useState(false);

  const checkValidation = () => {
    let formErrors = {};
    let formValid = true;
    if (!customerDetails.first_name) {
      formErrors["first_name"] = "Please enter first name.";
      formValid = false;
    } else if (customerDetails.first_name?.trim()?.length === 0) {
      formErrors["first_name"] = "Please enter valid first name.";
      formValid = false;
    } else if (customerDetails.first_name?.trim()?.length < 3) {
      formErrors["first_name"] = "Name should be more than 2 characters.";
      formValid = false;
    }
    if (!customerDetails.last_name) {
      formErrors["last_name"] = "Please enter last name.";
      formValid = false;
    } else if (customerDetails.last_name?.trim()?.length === 0) {
      formErrors["last_name"] = "Please enter valid last name.";
      formValid = false;
    } else if (customerDetails.last_name?.trim()?.length < 3) {
      formErrors["last_name"] = "Name should be more than 2 characters.";
      formValid = false;
    }
    if (!customerDetails.address) {
      formErrors["address"] = "Please select address.";
      formValid = false;
    }

    if (!customerDetails.phone) {
      formErrors["phone"] = "Please enter phone number.";
      formValid = false;
    } else if (customerDetails.phone.length !== 13) {
      formErrors["phone"] = "Please enter valid phone number.";
      formValid = false;
    }
    // if (!customerDetails.email || customerDetails.email === "") {
    //   formErrors["email"] = "Please enter email.";
    //   formValid = false;
    // } else if (!RAGEX.emailRegex.test(customerDetails.email)) {
    //   formErrors["email"] = "Please enter valid email.";
    //   formValid = false;
    // }
    if (!customerDetails.quantity) {
      formErrors["quantity"] = "Select your quantity.";
      formValid = false;
    }
    if (!customerDetails.item) {
      formErrors["item"] = "Select your item.";
      formValid = false;
    }
    if (!customerDetails.price) {
      formErrors["price"] = "Select your price.";
      formValid = false;
    }
    setFormError(formErrors);
    return formValid;
  };

  const handleSubmitListing = (e) => {
    e.preventDefault();
    if (checkValidation()) {
      userRegistration();
    } else {
      setShowValiErrorMsg(true);
      setTimeout(() => {
        setShowValiErrorMsg(false);
      }, 3000);
    }
  };

  async function userRegistration() {
    const newParam = {
      firstname: customerDetails.first_name,
      lastname: customerDetails.last_name,
      mobile: customerDetails.phone,
      address: customerDetails.address,
      items: JSON.stringify([
        {
          name: customerDetails.item,
          quantity: customerDetails.quantity,
          price: customerDetails.price,
        },
      ]),
      pinCode: customerDetails.zip_code,
    };

    try {
      const { data } = await apiCall(
        "POST",
        apiEndPoints.ADDCUSTOMER,
        newParam
      );

      if (data.status === 201) {
        toast.error(data.message, 2000);
      } else if (data.status == 200) {
        toast.success(data.message);
      } else {
        toast.error(data.message, 2000);
      }
    } catch (err) {
      console.log("err: ", err);
    }
  }

  return (
    <Fragment>
      <AddCustomer
        customerDetails={customerDetails}
        setCustomerDetails={setCustomerDetails}
        handleSubmitListing={handleSubmitListing}
        errorMsg={errorMsg}
        setErrorMsg={setErrorMsg}
        formError={formError}
        setFormError={setFormError}
        showValiErrorMsg={showValiErrorMsg}
      />
      {/* <AllCustomerList
      customerDetails={customerDetails}
      setCustomerDetails={setCustomerDetails}
      /> */}
    </Fragment>
  );
};

export default Index;
