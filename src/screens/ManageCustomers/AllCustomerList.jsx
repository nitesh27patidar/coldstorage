import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Breadcrumb from "component/common/breadcrumb";
import { Link, json, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import NoDataFound from "component/NoDataFound";
import base64 from "react-native-base64";
import { YELLOWLOADER } from "utils/constants";
import { getCustomerList } from "reduxStore/action/customerAction";


const AllCustomerList = () => {
    const dispatch = useDispatch();
    const cus_listSelector = useSelector((state) => state.getCustomerList);
    console.log("cus_listSelector", cus_listSelector);
    const navigatpage = useNavigate();
    const location_param = useParams();
    const [isLoading, setisLoading] = useState(false);
    const [customerList, setCustomerList] = useState([]);

    // console.log("customerList", customerList);

    useEffect(() => {
      if (cus_listSelector && cus_listSelector?.response?.status === 200) {
        console.log(cus_listSelector,"jjjjjjj")
        const modiItems = cus_listSelector?.response?.data.map((obj) => {
          console.log("obj.items name", obj.items);
          return { ...obj, items: obj.items ? JSON.parse(obj.items) : [] };
        });
        console.log(modiItems,"modiItems");
        setCustomerList(modiItems);
      }
    }, [cus_listSelector]);
  
    useEffect(() => {
      const item = {
        limit: 10 ,
        offset: 0,
        firstname: "",
      };
      dispatch(getCustomerList(item));
    }, []);

    const [form, setForm] = useState({
      start_date: null,
      end_date: null,
      search_customer: "",
      search_location: "",
      claim_status: "null",
      approve_status: "null",
    });

    const handleInputField = (e) => {
      const nextFormState = {
        ...form,
        [e.target.name]: e.target.value,
      };
      setForm(nextFormState);
    };

    console.log("form data", form);

    const filterBusiness = (e) => {
      e.preventDefault();
      const item = {
        limit: 10,
        offset: 0,
        firstname: form.search_customer,
      };

      dispatch(getCustomerList(item));
    };

    const handlePageClick = (event) => {
      window.scroll(0, 0);
    };

    const handleResetInput = () => {
      const item = {
        limit: 10,
        offset: 0,
        firstname: "",
      };

      dispatch(getCustomerList(item));
    };
    const handleDelete=()=>{
      
    }
  return (
    <div className="page-body">
      {/* Model_start */} 
      <div
        className="modal fade"
        id="exampleModal1"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel1"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabe2">
                Approved Business
              </h5>
              <button
                id="approve-modal"
                className="close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="text-center">
                <h6>Do you want to approve this business? </h6>
              </div>
            </div>
            <div className="modal-footer justify-content-center ">
              <button
                className="btn btn-success mr-5"
                type="button"
                data-dismiss="modal"
              >
                Yes
              </button>
              <button
                className="btn btn-primary"
                type="button"
                data-dismiss="modal"
              >
                No
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* Model_end */}
      <Breadcrumb
        title="Customer"
        parent="Manage Customer"
        titleactive="Customer List"
      />
      <div className="container-fluid">
        <div className="row">
          {!isLoading ? (
            <div className="col-sm-12 col-xl-12">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card">
                    <div className="card-body">
                      <form className="theme-form" onSubmit={filterBusiness}>
                        <div className="row">
                          <div className="col-md-6">
                            <span className="cardText">Date Range</span>
                            <div className="d-flex">
                              <div
                                className="input-group date"
                                id="dt-date"
                                data-target-input="nearest"
                              >
                                <DatePicker
                                  selected={
                                    form.start_date ? form.start_date : null
                                  }
                                  onKeyDown={(event) => event.preventDefault()}
                                  autoFocus={false}
                                  onChange={(date) =>
                                    setForm({
                                      ...form,
                                      start_date: date,
                                    })
                                  }
                                  dateFormat="MM-dd-yyyy"
                                  placeholderText="MM-DD-YYYY"
                                  showYearDropdown
                                  scrollableYearDropdown
                                  className="form-control form-control-md"
                                  // maxDate={today}
                                />
                              </div>
                              <div className="ml-2 mr-2  d-flex aic">To</div>
                              <div
                                className="input-group date"
                                id="dt-date"
                                data-target-input="nearest"
                              >
                                <DatePicker
                                  selected={
                                    form.end_date ? form.end_date : null
                                  }
                                  onKeyDown={(event) => event.preventDefault()}
                                  autoFocus={false}
                                  onChange={(date) =>
                                    setForm({
                                      ...form,
                                      end_date: date,
                                    })
                                  }
                                  dateFormat="MM-dd-yyyy"
                                  placeholderText="MM-DD-YYYY"
                                  showYearDropdown
                                  scrollableYearDropdown
                                  className="form-control form-control-md"
                                  // maxDate={today}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row pt-3">
                          <div className="col-md-4">
                            <span className="cardText">
                              Search by Customer Name
                            </span>
                            <div
                              className="input-group date"
                              id="dt-date"
                              data-target-input="nearest"
                            >
                              <input
                                className="form-control datetimepicker-input digits"
                                type="text"
                                autoComplete="off"
                                name="search_customer"
                                value={
                                  form.search_customer
                                    ? form.search_customer
                                    : ""
                                }
                                onChange={handleInputField}
                                // onChange={(e) => setForm({...form, search_customer: e.target.value})}
                                placeholder="Search Customer name"
                              />
                            </div>
                          </div>
                          <div className="col-md-4">
                            <span className="cardText">Location</span>
                            <div
                              className="input-group date"
                              id="dt-date"
                              data-target-input="nearest"
                            >
                              <input
                                className="form-control datetimepicker-input digits"
                                type="text"
                                autoComplete="off"
                                name="search_location"
                                value={
                                  form.search_location
                                    ? form.search_location
                                    : ""
                                }
                                onChange={handleInputField}
                                placeholder="Search location"
                              />
                            </div>
                          </div>
                          <div className="col-md-4 pt-4">
                            <div className="form-group d-flex justify-content-end">
                              <button
                                className="btn btn-primary mr-4"
                                type="submit"
                                onClick={filterBusiness}
                              >
                                Search
                              </button>
                              <button
                                className="btn btn-dark"
                                type="button"
                                // onClick={() =>
                                //   setForm({
                                //     start_date: null,
                                //     end_date: null,
                                //     search_business: null,
                                //     search_location: null,
                                //   })
                                // }
                                onClick={handleResetInput}
                              >
                                Reset
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12 col-xl-12">
                  <div className="card">
                    <div className="card-header">
                      <h5>Customer List</h5>
                    </div>
                    {customerList && customerList?.length > 0 ? (
                      <div>
                        <div className="table-responsive">
                          <table className="table table-border-horizontal">
                            <thead>
                              <tr>
                                <th scope="col" className="text-center">
                                  First Name
                                </th>
                                <th scope="col" className="text-center">
                                  Last Name
                                </th>
                                <th scope="col" className="text-center">
                                  Address
                                </th>
                                <th scope="col" className="text-center">
                                  Mobile No
                                </th>
                                <th scope="col" className="text-center">
                                  Items
                                </th>
                                <th scope="col" className="text-center">
                                  Action.
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {console.log("customerList mmmm", customerList)}
                              {customerList &&
                                customerList?.length > 0 &&
                                customerList.map((item, i) => {
                                  return (
                                    <tr key={i}>
                                      <td className="text-center">
                                        {item.firstname}
                                      </td>
                                      <td className="text-center">
                                        {item.lastname}
                                      </td>
                                      <td className="text-center">
                                        <div className="wrap_content">
                                          {/* {item.address?item.address:"-"} */}
                                          {item?.address
                                            ? item?.address?.length > 20
                                              ? item?.address?.substring(
                                                  0,
                                                  20
                                                ) + "..."
                                              : item?.address
                                            : "-"}
                                        </div>
                                      </td>

                                      <td className="text-center">
                                        {item.mobile}
                                      </td>

                                      {/* <td className="text-center">
                                        {item.items[0].name}
                                      </td> */}
                                      <td className="text-center">
                                        {item?.items && item?.items?.length > 0
                                          ? item.items.map(
                                              (itemObject, index) => (
                                                <span key={index}>
                                                  {itemObject.name}
                                                  {index !==
                                                    item.items.length - 1 &&
                                                    ", "}{" "}
                                                </span>
                                              )
                                            )
                                          : "No items"}
                                      </td>

                                      <td className="text-center p-2">
                                        <div class="dropdown">
                                          <button
                                            class="btn btn-secondary dropdown-toggle"
                                            type="button"
                                            id="dropdownMenu2"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                          >
                                            Dropdown
                                          </button>
                                          <div
                                            class="dropdown-menu"
                                            aria-labelledby="dropdownMenu2"
                                          >
                                            <button
                                              class="dropdown-item"
                                              type="button"
                                            >
                                              
                                              <Link
                                                className="dropdown-item"
                                                to={`/customers/view-customer/${item.id}`}
                                              >
                                                View
                                              </Link>
                                            </button>
                                            <button
                                              class="dropdown-item"
                                              type="button"
                                            >
                                              <Link
                                                className="dropdown-item"
                                                to={`/customers/add-items/${item.id}`}
                                              >
                                                Add Items
                                              </Link>
                                            
                                            </button>
                                            <button
                                              class="dropdown-item"
                                              type="button"
                                            >
                                              Edit Items
                                            </button>
                                            <button
                                              class="dropdown-item"
                                              type="button"
                                              onClick={()=>handleDelete(item.id)}
                                            >
                                              Delete Items
                                            </button>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  );
                                })}
                            </tbody>
                          </table>
                          {/* <TableDataNotFound data={supportFormData} /> */}
                        </div>
                        <hr />{" "}
                        <div className="row">
                          <div class="col-12">
                            <div class="card ">
                              <div class="card-body ">
                                <div class="pagination pagination-primary float-right">
                                  {/* <ReactPaginate
                                  previousLabel={"Previous"}
                                  nextLabel={"Next"}
                                  breakLabel={"..."}
                                  breakClassName={"break-me"}
                                  pageCount={pageCount} 
                                  onPageChange={(e) => handlePageClick(e)}
                                  containerClassName={"pagination"}
                                  subContainerClassName={"pages pagination"}
                                  activeClassName={"active"}
                                  pageClassName={"page-item"}
                                  pageLinkClassName={"page-link"}
                                  previousClassName={"page-item"}
                                  nextClassName={"page-item"}
                                  previousLinkClassName={"page-link"}
                                  nextLinkClassName={"page-link"}
                                  forcePage={page_no}
                                
                                /> */} 
                                  <ReactPaginate
                                    breakLabel="..."
                                    nextLabel="next >"
                                    onPageChange={(e) => handlePageClick(e)}
                                    // pageCount={pageCount}
                                    previousLabel="< previous"
                                    pageClassName="page-item"
                                    pageLinkClassName="page-link"
                                    previousClassName="page-item"
                                    previousLinkClassName="page-link"
                                    nextClassName="page-item"
                                    nextLinkClassName="page-link"
                                    breakClassName="page-item"
                                    breakLinkClassName="page-link"
                                    containerClassName="pagination"
                                    activeClassName="active"
                                    // forcePage={Number(page_no)}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <NoDataFound />
                    )}
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="m-loader">
              <img
                src={YELLOWLOADER}
                style={{ width: "90px", marginBottom: "150px" }}
              />
            </div>
          )}
        </div>
      </div>
     {/* <ViewCustomer customerList={customerList}/> */}
    </div>
    
  );
};

  



export default AllCustomerList;
