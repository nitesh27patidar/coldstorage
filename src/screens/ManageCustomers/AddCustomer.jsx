import React, { Fragment, useEffect } from "react";
import Geocode from "react-geocode";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import PhoneInput from "react-phone-number-input";
import { RAGEX } from "utils/constants";

Geocode.setApiKey("AIzaSyCbDx7Lk4eTMzptrQKXZvOPYgEMggrq8o4");
 
const AddCustomer = (props) => {
  async function getAddressObject(address_components, fulladdress, lat, lng) {
    var ShouldBeComponent = {
      home: ["street_number"],
      postal_code: ["postal_code"],
      street: ["street_address", "route"],
      region: [
        "administrative_area_level_1",
        "administrative_area_level_2",
        "administrative_area_level_3",
        "administrative_area_level_4",
        "administrative_area_level_5",
      ],
      city: [
        "locality",
        "sublocality",
        "sublocality_level_1",
        "sublocality_level_2",
        "sublocality_level_3",
        "sublocality_level_4",
      ],
      country: ["country"],
    };

    var address = {
      home: "",
      postal_code: "",
      street: "",
      region: "",
      city: "",
      country: "",
    };
    address_components.forEach((component) => {
      for (var shouldBe in ShouldBeComponent) {
        if (ShouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1) {
          address[shouldBe] = component.long_name;
        }
      }
    });

    await props.setCustomerDetails({
      ...props.customerDetails,
      address: fulladdress,
      latitude: lat,
      longitude: lng,
      zip_code: address.postal_code,
      region: address.region,
      country: address.country,
      city: address.city,
    });
  }

   const handleChangeAddress = (address) => {
    props.setCustomerDetails({ ...props.customerDetails, address: address });
  };

  const handleSelectAddress = (address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        props.setCustomerDetails({
          ...props.customerDetails,
          address: address,
          latitude: latLng.lat,
          longitude: latLng.lng,
        });
        Geocode.fromLatLng(latLng.lat, latLng.lng).then(
          (response) => {
            getAddressObject(
              response.results[0].address_components,
              address,
              latLng.lat,
              latLng.lng
            );
          },
          (error) => {
            console.log("error", error);
          }
        );
      });
  };

  return (
    <Fragment>
      <div className="page-body">
        <div className="container-fluid">
          <div className="page-header">
            <div className="row">
              <div className="col">
                <div className="page-header-left">
                  <h3>Business</h3>
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                      <a>
                        <i
                          className="fa fa-home theme-fa-icon"
                          aria-hidden="true"
                          // onClick={() => navigatpage("/dashboard")}
                        ></i>
                      </a>
                    </li>
                    <li className="breadcrumb-item">Manage Customers</li>
                    <li className="breadcrumb-item active">Add New Customer</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row ">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-header">
                  <h5>Enter Customer Details</h5>
                </div>
                <div className="im-biz-detail-container">
                  <div>
                    <div className="im-detail-container">
                      <form>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            First Name
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              pattern="^[^\s]+$"
                              title="Space is not allowed."
                              autoComplete="off"
                              className={`form-control ${
                                !props.customerDetails.first_name &&
                                props.formError &&
                                props.formError.first_name &&
                                "error-border-red"
                              }`}
                              onChange={(e) => {
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  first_name: e.target.value,
                                });
                                props.setFormError({
                                  ...props.formError,
                                  first_name: "",
                                });
                              }}
                              value={
                                props.customerDetails &&
                                props.customerDetails?.first_name
                              }
                              name="first_name"
                              maxLength={40}
                            />
                            {(!props.customerDetails.first_name ||
                              props.customerDetails.first_name.trim()
                                ?.length === 0 ||
                              props.customerDetails.first_name.trim()?.length <
                                3) &&
                              props.formError?.first_name && (
                                <span className style={{ color: "red" }}>
                                  {props.formError?.first_name}
                                </span>
                              )}
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Last Name
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              pattern="^[^\s]+$"
                              title="Space is not allowed."
                              autoComplete="off"
                              className={`form-control ${
                                !props.customerDetails.last_name &&
                                props.formError &&
                                props.formError.last_name &&
                                "error-border-red"
                              }`}
                              onChange={(e) => {
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  last_name: e.target.value,
                                });
                                props.setFormError({
                                  ...props.formError,
                                  last_name: "",
                                });
                              }}
                              value={
                                props.customerDetails &&
                                props.customerDetails?.last_name
                              }
                              name="last_name"
                              maxLength={40}
                            />
                            {(!props.customerDetails.last_name ||
                              props.customerDetails.last_name.trim()?.length ===
                                0 ||
                              props.customerDetails.last_name.trim()?.length <
                                3) &&
                              props.formError?.last_name && (
                                <span className style={{ color: "red" }}>
                                  {props.formError?.last_name}
                                </span>
                              )}
                          </div>
                        </div>

                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Address
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <PlacesAutocomplete
                              value={props.customerDetails.address}
                              onChange={handleChangeAddress}
                              onSelect={handleSelectAddress}
                            >
                              {({
                                getInputProps,
                                suggestions,
                                getSuggestionItemProps,
                                loading,
                              }) => (
                                <div className="header-select-location">
                                  <input
                                    {...getInputProps({
                                      placeholder: "Search Places ...",
                                      className: "form-control",
                                    })}
                                    style={{
                                      borderRadius: "0px",
                                    }}
                                    className={`form-control ${
                                      !props.customerDetails.address &&
                                      props.formError &&
                                      props.formError.address &&
                                      "error-border-red"
                                    }`}
                                  />
                                  <div className="autoComplete-dropdown-container autosearch_dropdown_on_header">
                                    {loading && (
                                      <div
                                        style={{
                                          textAlign: "center",
                                          padding: "7px",
                                        }}
                                      >
                                        <img
                                          src="./assets/images/loader.gif"
                                          style={{
                                            width: "25px",
                                            height: "25px",
                                          }}
                                        />
                                      </div>
                                    )}
                                    {suggestions.map((suggestion) => {
                                      const className = suggestion.active
                                        ? "suggestion-item--active"
                                        : "suggestion-item";
                                      const style = suggestion.active
                                        ? {
                                            backgroundColor: "#fafafa",
                                            cursor: "pointer",
                                          }
                                        : {
                                            backgroundColor: "#ffffff",
                                            cursor: "pointer",
                                          };
                                      return (
                                        <div
                                          {...getSuggestionItemProps(
                                            suggestion,
                                            {
                                              className,
                                              style,
                                            }
                                          )}
                                        >
                                          <span>{suggestion.description}</span>
                                        </div>
                                      );
                                    })}
                                  </div>
                                </div>
                              )}
                            </PlacesAutocomplete>
                            {!props.customerDetails.address &&
                              props.formError?.address && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.address}
                                </span>
                              )}
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            PinCode
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              autoComplete="off"
                              // maxLength={6}
                              value={props.customerDetails?.zip_code}
                              onChange={(e) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  zip_code: e.target.value,
                                })
                              }
                              className={`form-control ${
                                !props.customerDetails.zip_code &&
                                props.formError &&
                                props.formError.zip_code &&
                                "error-border-red"
                              }`}
                              onKeyPress={(event) => {
                                if (/^[a-z]+$/.test(event.key)) {
                                  event.preventDefault();
                                }
                              }}
                            />
                            {!props.customerDetails.zip_code &&
                              props.formError &&
                              props.formError.zip_code && (
                                <span style={{ color: "red" }}>
                                  {props.formError.zip_code}
                                </span>
                              )}
                          </div>
                        </div>
                        <div className="form-group row">
                          <label
                            for="inputtext3"
                            className="col-sm-2 col-form-label"
                          >
                            Mobile No.<span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <PhoneInput
                              placeholder="Enter phone number"
                              className={`cus-phone-inp p-3 form-control ${
                                (!props.customerDetails.phone ||
                                  props.customerDetails.phone.length < 10 ||
                                  props.customerDetails.phone.length > 13) &&
                                props.importURL == "" &&
                                props.formError?.phone &&
                                "error-border-red"
                              }`}
                              maxLength="14"
                              value={props?.buisnessDetails?.phone}
                              defaultCountry="IN"
                              onChange={(value) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  phone: value,
                                })
                              }
                              style={{ display: "flex" }}
                            />
                            {(!props.customerDetails.phone ||
                              props.customerDetails.phone.length !== 12) &&
                              props.formError?.phone && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.phone}
                                </span>
                              )}
                          </div>
                        </div>
                         {/* <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Email
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="email"
                              autoComplete="off"
                              // className={`form-control ${
                              //   !props.customerDetails.email &&
                              //   props.formError &&
                              //   props.formError.email &&
                              //   "error-border-red"
                              // }`}
                              className={
                                (!props.customerDetails.email ||
                                  !RAGEX.emailRegex.test(
                                    props.customerDetails.email
                                  )) &&
                                props.formError?.email
                                  ? "form-control error-border-red"
                                  : "form-control"
                              }
                              value={props.customerDetails?.email}
                              onChange={(e) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  email: e.target.value,
                                })
                              }
                              // readOnly
                              // disabled
                            />
                            {(!props.customerDetails.email ||
                              props.customerDetails.email === "" ||
                              !RAGEX.emailRegex.test(
                                props.customerDetails.email
                              )) &&
                              props.formError?.email && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.email}
                                </span>
                              )}
                          </div>
                        </div>  */}

                       {/* <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Item<span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              autoComplete="off"
                              // className={`form-control ${
                              //   !props.customerDetails.email &&
                              //   props.formError &&
                              //   props.formError.email &&
                              //   "error-border-red"
                              // }`}
                              className={
                                !props.customerDetails.item &&
                                props.formError?.item
                                  ? "form-control error-border-red"
                                  : "form-control"
                              }
                              value={props.customerDetails?.item}
                              onChange={(e) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  item: e.target.value,
                                })
                              }
                              // readOnly
                              // disabled
                            />
                            {(!props.customerDetails.item ||
                              props.customerDetails.item === "") &&
                              props.formError?.item && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.item}
                                </span>
                              )}
                          </div>
                        </div>  */}
                        {/* <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Quantity <br/>(In KG)<span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              autoComplete="off"
                              className={
                                !props.customerDetails.quantity &&
                                props.formError?.quantity
                                  ? "form-control error-border-red"
                                  : "form-control"
                              }
                              value={props.customerDetails?.quantity}
                              onChange={(e) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  quantity: e.target.value,
                                })
                              }
                            />
                            {(!props.customerDetails.quantity ||
                              props.customerDetails.quantity === "") &&
                              props.formError?.quantity && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.quantity}
                                </span>
                              )}
                          </div>
                        </div> */}
                        {/* <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Price<span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              autoComplete="off"
                              className={
                                !props.customerDetails.price &&
                                props.formError?.price
                                  ? "form-control error-border-red"
                                  : "form-control"
                              }
                              value={props.customerDetails?.price}
                              onChange={(e) =>
                                props.setCustomerDetails({
                                  ...props.customerDetails,
                                  price: e.target.value,
                                })
                              }
                              // readOnly
                              // disabled
                            />
                            {(!props.customerDetails.price ||
                              props.customerDetails.price === "") &&
                              props.formError?.price && (
                                <span style={{ color: "red" }}>
                                  {props.formError?.price}
                                </span>
                              )}
                          </div>
                        </div> */}
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label"></label>
                          <div className="col-sm-10">
                            <button
                              className="btn btn-block btn-primary"
                              onClick={props.handleSubmitListing}
                              style={{ width: "200px" }}
                            >
                              Add Customer
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default AddCustomer;
