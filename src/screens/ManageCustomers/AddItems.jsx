import React, { Fragment } from "react";

function AddItems() {
  return (
    <>
      <Fragment>
        <div className="page-body">
          <div className="container-fluid">
            <div className="page-header">
              <div className="row">
                <div className="col">
                  <div className="page-header-left">
                    <h3>Customer</h3>
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <a>
                          <i
                            className="fa fa-home theme-fa-icon"
                            aria-hidden="true"
                            // onClick={() => navigatpage("/dashboard")}
                          ></i>
                        </a>
                      </li>
                      <li className="breadcrumb-item">Manage Customers</li>
                      <li className="breadcrumb-item active">
                        Add New Customer
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <p>Customer Details for ID:</p>
          <div className="container-fluid">
            <div className="row ">
              <div className="col-sm-12">
                <div className="card">
                  <div className="card-header">
                    <h5>Add Item </h5>
                  </div>
                  <div className="im-biz-detail-container">
                    <div>
                      <div className="im-detail-container">
                        <form>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              First Name: <span>Nitesh</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Last Name: <span>--</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Email: <span>@gmail.com</span>{" "}
                            </label>
                          </div>

                          <div
                            className="form-group row  "
                            style={{ display: "flex" }}
                          >
                            <label
                              for="inputtext3"
                              className="col-sm-0 col-form-label"
                            >
                              Add Items
                            </label>
                            <div className="col-sm-3">
                              <input className="cus-phone-inp  form-control" />
                            </div>
                            <label
                              for="inputtext3"
                              className="col-sm-0 col-form-label"
                            >
                              Quantity
                            </label>
                            <div className="col-sm-3">
                              <input className="cus-phone-inp  form-control" />
                            </div>
                            <label
                              for="inputtext3"
                              className="col-sm-0 col-form-label"
                            >
                              Price
                            </label>
                            <div className="col-sm-3">
                              <input className="cus-phone-inp  form-control" />
                            </div>
                          </div>
                          <button
                            className="btn btn-block btn-primary mb-3"
                            style={{ width: "200px", marginLeft: "300px" }}
                          >
                            Add item
                          </button>

                          {/* <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Items: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Quantity: <span>hhjgf</span>{" "}
                            </label>
                          </div>
                          <div className="form-group row">
                            <label className="col-sm-10 col-form-label border border-2">
                              Price: <span>hhjgf</span>{" "}
                            </label>
                          </div> */}
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    </>
  );
}

export default AddItems;
