import React, { useState, useEffect, Fragment } from "react";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import { setDefaultHeader } from "../../../utils/httpClient";
import { useDispatch, useSelector } from "react-redux";
import { jwtTokenGenrate, userLogin } from "reduxStore/action/authActions";
import Login from "./Login";
const Signin = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const loginSelector = useSelector((state) => state.login)
  const [errorMessage, setErrorMessage] = useState({});
  const [passwordShown, setPasswordShown] = useState(false);
  const [authData, setAuthData] = useState({});

  useEffect(() => {
    checklogin();
  },[loginSelector])


  const checklogin = async () => {
    if (loginSelector.response && loginSelector.authToken) {
      if (loginSelector.response.status === 200) {
        await setDefaultHeader("token", loginSelector.response.token);
        navigate("/dashboard");
      }
    } 
    // else {
    //   if (loginSelector.response == null) {
    //     dispatch(jwtTokenGenrate());
    //   } else {
    //     await setDefaultHeader("token", loginSelector.response.token);
    //   }
    // }
  };

  const handleEnterPressFromSecond = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      handleAuthentication()
    }
  };

  const togglePassword = () => {
    // When the handler is invoked
    // inverse the boolean state of passwordShown
    setPasswordShown(!passwordShown);
  };

  const handleAuthentication = () => {
    if(authData?.email?.length > 0 && authData?.password?.length > 0 ){
      dispatch(userLogin(authData))      
    }
  }

  return (
    <Fragment>
      <Login
        errorMessage={errorMessage}
        passwordShown={passwordShown}
        handleEnterPressFromSecond={handleEnterPressFromSecond}
        togglePassword={togglePassword}
        authData={authData}
        setAuthData={setAuthData}
        handleAuthentication={handleAuthentication}
      />
    </Fragment>
  );
};

export default Signin;
