import React, { Fragment } from "react";
import { LOGO } from "utils/constants";
import { useNavigate } from "react-router-dom";

const Login = (props) => {
  const navigate = useNavigate();
  const {
    errorMessage,
    passwordShown,
    handleEnterPressFromSecond,
    togglePassword,
    authData,
    setAuthData,
    handleAuthentication
  } = props;
  return (
    <Fragment>
      <div className="page-wrapper">
        <div className="container-fluid p-0">
          {/* login page start*/}
          <div className="authentication-main">
            <div className="row">
              <div className="col-md-12">
                <div className="auth-innerright">
                  <div className="authentication-box">
                    <div className="text-center">
                      <img
                        className="login-logo"
                        // src="/assets/images/abbylogo-YLWWHT400x160.png"
                        src={LOGO}
                        alt=""
                      />
                    </div>
                    <div className="card mt-4">
                      <div className="card-body">
                        <div className="text-center">
                          <h4>LOGIN</h4>
                          <h6>Enter Your Email And Password </h6>
                        </div>
                        <form className="theme-form">
                          <div>
                            <label className="col-form-label pt-0">
                            Email
                            </label>
                            <input
                              className="form-control"
                              autoComplete="off"
                              id="focus_1"
                              type="text"
                              onKeyDown={handleEnterPressFromSecond}
                              required
                              onChange={(e) =>
                                setAuthData({
                                  ...authData,
                                  email: e.target.value,
                                })
                              }
                            />
                          </div>
                          <div>
                            <label className="col-form-label pt-2">
                              Password
                            </label>
                          </div>
                          <div className="input-group">
                            <input
                              className="form-control"
                              autoComplete="off"
                              id="focus_2"
                              type={passwordShown ? "text" : "password"}
                              required
                              onKeyDown={handleEnterPressFromSecond}
                              style={{
                                backgroundColor: "#e8f0fe",
                                borderRight: "none",
                              }}
                              onChange={(e) =>
                                setAuthData({
                                  ...authData,
                                  password: e.target.value,
                                })
                              }
                            />
                            <div className="input-group-append">
                              <span
                                className="input-group-text"
                                style={{
                                  backgroundColor: "#e8f0fe",
                                  borderLeft: "none",
                                }}
                              >
                                <i
                                  style={{
                                    left: "370px",
                                    fontSize: "20px",
                                    top: "6px",
                                  }}
                                  className={
                                    passwordShown
                                      ? "fa fa-eye"
                                      : "fa fa-eye-slash"
                                  }
                                  onClick={togglePassword}
                                  aria-hidden="true"
                                ></i>
                              </span>
                            </div>
                          </div>
                          <small style={{ color: "red", fontSize: "13px" }}>
                            {errorMessage?.password}
                          </small>
                          <div className="form-group form-row mt-2 mb-0 pt-4">
                            <button
                              style={{
                                fontWeight: "600",
                                fontSize: "15px",
                              }}
                              onClick={() => handleAuthentication()}
                              className="btn btn-block btn-primary"
                              type="button"
                            >
                              Login
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* login page end*/}
        </div>
      </div>
    </Fragment>
  );
};

export default Login;
