import Breadcrumb from 'component/common/breadcrumb'
import React from 'react'

function Profile() {
  return (
    <>
    <div className="page-body">
    <Breadcrumb
          title="AddUsers"
          parent="Manage User"
          titleactive="Add User"
        />
    </div>
    </>
  )
}

export default Profile