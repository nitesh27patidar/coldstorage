import Breadcrumb from "component/common/breadcrumb";
import moment from "moment";
import React, { Fragment, useState } from "react";
import PhoneInput from "react-phone-number-input";
import PlacesAutocomplete from "react-places-autocomplete";
import Select from "react-select";

function AddNewUser(props) {
  const {
    options,
    userInfo,
    setUserInfo,
    handlesumbit,
    formErr,
    id,
    Role,
    handleupdate,
  } = props;

  const {
    firstname,
    lastname,
    username,
    email,
    mobile,
    gender,
    userrole,
    DOB,
  } = userInfo;
  console.log("🚀 ~ AddNewUser ~ userInfo:", userInfo);

  return (
    <>
      <div className="page-body">
        <Breadcrumb
          title="AddUsers"
          parent="Manage User"
          titleactive="Add User"
        />

        <div className="container-fluid">
          <div className="row ">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-header">
                  <h5>Enter Customer Details</h5>
                </div>
                <div className="im-biz-detail-container">
                  <div>
                    <div className="im-detail-container">
                      <form>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            First Name
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              title="Space is not allowed."
                              autoComplete="off"
                              className="form-control"
                              // name="first_name"
                              style={{textTransform:"capitalize"}}
                              value={firstname}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  firstname: e.target.value,
                                })
                              }
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.firstname &&
                              formErr &&
                              formErr.firstname
                                ? formErr.firstname
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Last Name
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              title="Space is not allowed."
                              autoComplete="off"
                              className="form-control"
                              style={{textTransform:"capitalize"}}
                              value={lastname}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  lastname: e.target.value,
                                })
                              }
                              name="last_name"
                              maxLength={40}
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.lastname && formErr && formErr.lastname
                                ? formErr.lastname
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            UserName
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="text"
                              title="Space is not allowed."
                              autoComplete="off"
                              className="form-control"
                              value={username}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  username: e.target.value,
                                })
                              }
                              name="last_name"
                              maxLength={40}
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.username && formErr && formErr.username
                                ? formErr.username
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Email
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                              type="email"
                              pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                              className="form-control"
                              value={email}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  email: e.target.value,
                                })
                              }
                              name="email"
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.email && formErr && formErr.email
                                ? formErr.email
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Gender
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <div className="form-check">
                              <input
                                type="radio"
                                id="option2"
                                name="options"
                                value={1}
                                checked={userInfo.gender === 1 ? true : false}
                                className="form-check-input"
                                onChange={(e) =>
                                  setUserInfo({
                                    ...userInfo,
                                    gender: Number(e.target.value),
                                  })
                                }
                              />

                              <label
                                className="form-check-label mr-5"
                                htmlFor="option2"
                              >
                                Male
                              </label>
                              <input
                                type="radio"
                                id="option1"
                                name="options"
                                value={2}
                                checked={userInfo.gender === 2 ? true : false}
                                className="form-check-input"
                                onChange={(e) =>
                                  setUserInfo({
                                    ...userInfo,
                                    gender: Number(e.target.value),
                                  })
                                }
                              />
                              <label
                                className="form-check-label"
                                htmlFor="option1"
                              >
                                Female
                              </label>
                            </div>
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.gender && formErr && formErr.gender
                                ? formErr.gender
                                : ""}
                            </span>
                          </div>
                        </div>

                        <div className="form-group row">
                          <label
                            for="inputtext3"
                            className="col-sm-2 col-form-label"
                          >
                            User Role
                            <span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <Select
                              value={
                                userInfo?.userrole
                                  ? {
                                      label: userInfo?.userrole,
                                      value: userInfo?.role_id,
                                    }
                                  : null
                              }
                              // value={options?.label}
                              onChange={(selectedOption) =>
                                setUserInfo({
                                  ...userInfo,
                                  userrole: selectedOption.label,
                                })
                              }
                              options={options}
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.userrole && formErr && formErr.userrole
                                ? formErr.userrole
                                : ""}
                            </span>
                          </div>
                        </div>

                        <div className="form-group row">
                          <label
                            for="inputtext3"
                            className="col-sm-2 col-form-label"
                          >
                            Mobile No.<span className="required-field"></span>
                          </label>
                          <div className="col-sm-10">
                            <input
                            // type="number"
                              placeholder="Enter phone number"
                              className="cus-phone-inp p-3 form-control"
                              maxLength="10"
                              value={mobile}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  mobile: e.target.value, 
                                })
                              }
                              defaultCountry="IN"
                              style={{ display: "flex" }}
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.mobile && formErr && formErr.mobile
                                ? formErr.mobile
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label">
                            Date Of Birth
                            <span className="required-field"></span>
                          </label>

                          <div className="col-sm-10">
                            <input
                              type="date"
                              value={moment(userInfo.DOB).format("YYYY-MM-DD")}
                              onChange={(e) =>
                                setUserInfo({
                                  ...userInfo,
                                  DOB: e.target.value,
                                })
                              }
                              className="form-control"
                            />
                            <span
                              style={{
                                color: "red",
                                fontSize: "10px",
                              }}
                            >
                              {!userInfo.DOB && formErr && formErr.DOB
                                ? formErr.DOB
                                : ""}
                            </span>
                          </div>
                        </div>

                        <div className="form-group row">
                          <label className="col-sm-2 col-form-label"></label>
                          <div className="col-sm-10">
                            {id > 0 ? (
                              <button
                                className="btn btn-block btn-primary"
                                onClick={handleupdate}
                                style={{ width: "200px" }}
                              >
                                Update User
                              </button>
                            ) : (
                              <button
                                className="btn btn-block btn-primary"
                                onClick={handlesumbit}
                                style={{ width: "200px" }}
                              >
                                Add User
                              </button>
                            )}

                            {/* <button
                      className="btn btn-primary"
                      onClick={""}
                    >
                     Back
                    </button> */}
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AddNewUser;
