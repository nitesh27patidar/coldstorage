import React, { useEffect, useState } from "react";
import AddNewUser from "./component/AddNewUser";
import { useDispatch, useSelector } from "react-redux";
import {
  AddUser,
  EditUser,
  UserDetails,
  UserRole,
  resetState,
} from "reduxStore/action/userAction";
import { useParams, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { errorToast } from "utils/httpClient";

function Index() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const userDetail = useSelector((state) => state.User);
  console.log("🚀 ~ Index ~ userDetail:", userDetail);
  const RoleData = useSelector((state) => state.User);
  console.log("🚀 ~ Index ~ RoleData:", RoleData);
  const AddUserRes = useSelector((state) => state.User);

  const moment = require("moment");
  const navigate = useNavigate();

  const [userInfo, setUserInfo] = useState({
    id: parseInt(id),
    firstname: "",
    lastname: "",
    username: "",
    email: "",
    mobile: "",
    gender: 0,
    status: 1,
    userrole: 1,
    role_id: 2,
    DOB: "",
  });
  const [formErr, setFormErr] = useState({});

  useEffect(() => {
    if (
      userDetail &&
      Number(id) > 0 &&
      userDetail?.userDetails?.status === 200
    ) {
      const result = userDetail?.userDetails?.data[0];

      // console.log("result===> ",formateddate  );
      setUserInfo({
        ...userInfo,
        firstname: result.firstname,
        lastname: result.lastname,
        username: result.username,
        email: result.email,
        mobile: result.mobile,
        gender: result.gender,
        status: result.status,
        userrole: result.userrole,
        DOB: result.DOB,
      });
    } else {
      setUserInfo({
        ...userInfo,
        firstname: "",
        lastname: "",
        username: "",
        email: "",
        mobile: "",
        gender: 0,
        status: 1,
        userrole: 0,
        DOB: "",
      });
    }
  }, [userDetail]);

  useEffect(() => {
    if (id && id > 0) {
      const param = {
        id: id,
      };
      dispatch(UserDetails(param));
    }
  }, [id]);

  useEffect(() => {
    const item = {
      title: "",
      type: "ROLE",
      limit: 10,
      offset: 0,
    };
    dispatch(UserRole(item));
  }, []);

  const Role = RoleData?.responseUserRole?.data;

  const options = Array.isArray(Role)
    ? Role.map((role) => ({
        value: role?.master_id,
        label: role?.title,
      }))
    : [];
  console.log(options, "options===>");

  const handleupdate = async (event) => {
    event.preventDefault();
    emailValidation();
    try {
      const param = {
        id: id,
        firstname: userInfo.firstname,
        lastname: userInfo.lastname,
        username: userInfo.username,
        email: userInfo.email,
        mobile: userInfo.mobile,
        gender: userInfo.gender,
        status: userInfo.status,
        userrole: userInfo.role_id,
        DOB: moment(userInfo.DOB).format("YYYY-MM-DD"),
      };

      await dispatch(EditUser(param));
      navigate(-1);
    } catch (err) {}
  };

  const handlesumbit = async (event) => {
    event.preventDefault();
    emailValidation();
    try {
      const param = {
        firstname: userInfo.firstname,
        lastname: userInfo.lastname,
        username: userInfo.username,
        email: userInfo.email,
        mobile: userInfo.mobile,
        gender: userInfo.gender,
        status: userInfo.status,
        userrole: userInfo.role_id,
        DOB: moment(userInfo.DOB).format("YYYY-MM-DD"),
      };
      await dispatch(AddUser(param));
      if (errorToast == "") {
        navigate(-1);
      } else {
      }
    } catch (err) {
      // errorToast('An error occurred. Please try again later.');
    }
  };

  console.log(userInfo, "=======>");
  function emailValidation() {
    console.log("userInfo.mobile", userInfo.mobile.length);
    let formErr = {};
    let formIsValid = true;
    if (!userInfo.firstname) {
      formIsValid = false;
      formErr["firstname"] = "Enter Your Name";
    }
    if (!userInfo.lastname) {
      formIsValid = false;
      formErr["lastname"] = "Enter Your Name";
    }
    if (!userInfo.username) {
      formIsValid = false;
      formErr["username"] = "Enter the UsernName";
    }
    if (!userInfo.email) {
      formIsValid = false;
      formErr["email"] = "Enter your Email";
    }
    if (!userInfo.mobile) {
      formIsValid = false;
      formErr["mobile"] = "Enter your Mobile Number";
    }
    if (userInfo.mobile.length < 10) {
      userInfo.mobile = "";
      formIsValid = false;
      formErr["mobile"] = "Enter 10 digit mobile number";
    }

    if (!userInfo.gender) {
      formIsValid = false;
      formErr["gender"] = "Select your Gender";
    }
    if (!userInfo.userrole) {
      formIsValid = false;
      formErr["userrole"] = "Select your Role";
    }
    if (!userInfo.DOB) {
      formIsValid = false;
      formErr["DOB"] = "Select your Date Of Birth";
    }
    setFormErr(formErr);
    return formIsValid;
  }

  return (
    <>
      <AddNewUser
        options={options}
        // selectedOption={selectedOption}
        // setSelectedOption={setSelectedOption}

        userInfo={userInfo}
        setUserInfo={setUserInfo}
        handlesumbit={handlesumbit}
        handleupdate={handleupdate}
        formErr={formErr}
        setFormErr={setFormErr}
        id={id}
        Role={Role}
      />
    </>
  );
}

export default Index;
