import Select from "react-select";
import Breadcrumb from "component/common/breadcrumb";
import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";
import { YELLOWLOADER } from "utils/constants";
import Pagination from "component/common/Pagination";

function UserList(props) {
  const {
    searchValue,
    setSearchValue,
    handleSearch,
    options,
    userData,
    itemLimit,
    totalrecord,
    itemOffset,
    setItemOffset,
    currentPost,
    postPerPage,
    totalPost,
    handleSetCurrentPage,
    handleNext,
    handlePrevious,
    totalPages,
    currentPage,
  } = props;
  const { firstname, role_id } = searchValue;
  const [isLoading, setisLoading] = useState(false);

  return (
    <>
      <div className="page-body">
        <Breadcrumb
          title="Users"
          parent="Manage Customer"
          titleactive="Users List"
        />
        <div className="container-fluid">
          <div className="row">
            {!isLoading ? (
              <div className="col-sm-12 col-xl-12">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="card">
                      <div className="card-body">
                        <form className="theme-form">
                          <div className="row ">
                            <div className="col-md-4">
                              <span className="cardText">
                                Search by User Name
                              </span>
                              <div
                                className="input-group date"
                                id="dt-date"
                                data-target-input="nearest"
                              >
                                <input
                                  className="form-control datetimepicker-input digits"
                                  type="text"
                                  autoComplete="off"
                                  name="search_customer"
                                  value={searchValue.firstname}
                                  onChange={(e) =>
                                    setSearchValue({
                                      ...searchValue,
                                      firstname: e.target.value,
                                    })
                                  }
                                  placeholder="Search User name"
                                />
                              </div>
                            </div>
                            <div className="col-md-4">
                              <span className="cardText">
                                Search By UserRole
                              </span>
                              <div
                                className="input-group date"
                                id="dt-date"
                                data-target-input="nearest"
                              >
                                {/* <Select
                                  value={options.label}
                                  onChange={(selectedOption) =>
                                    setSearchValue({
                                      ...searchValue,
                                      role_id: selectedOption.value,
                                    })
                                  }
                                  options={options}
                                  placeholder="Select by UserRole"
                                /> */}
                                <Select
                                  value={options?.label}
                                  onChange={(selectedOption) =>
                                    setSearchValue({
                                      ...searchValue,
                                      role_id: selectedOption.value,
                                    })
                                  }
                                  options={options}
                                  placeholder="Select by UserRole"
                                />
                              </div>
                            </div>
                            <div className="col-md-4 pt-4">
                              <div className="form-group d-flex justify-content-end">
                                <button
                                  className="btn btn-primary  mr-4"
                                  type="button"
                                  onClick={handleSearch}
                                >
                                  Search
                                </button>
                                <button className="btn btn-dark">Reset</button>
                              </div>
                            </div>
                          </div>

                          <div className="d-flex justify-content-end ">
                            <Link
                              className="btn btn-primary "
                              to="/ManageUsers/AddNewUser/0"
                            >
                              &nbsp; &nbsp; Add New User &nbsp; &nbsp;
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 col-xl-12">
                    <div className="card">
                      <div className="card-header">
                        <h5>Users List</h5>
                      </div>
                      <div className="table-responsive">
                        <table className="table table-border-horizontal">
                          <colgroup>
                            <col style={{ width: "18%" }} /> {/* Adjust the width values as needed */}
                            <col style={{ width: "18%" }} />
                            <col style={{ width: "18%" }} />
                            <col style={{ width: "18%" }} />
                            <col style={{ width: "18%" }} />
                            <col style={{ width: "9%" }} />
                          </colgroup>
                          <thead>
                            <tr>
                              <th scope="col" className="text-center">
                                First Name
                              </th>
                              <th scope="col" className="text-center">
                                Last Name
                              </th>
                              <th scope="col" className="text-center">
                                UserRole
                              </th>
                              <th scope="col" className="text-center">
                                Mobile No
                              </th>
                              <th scope="col" className="text-center">
                                Items
                              </th>
                              <th scope="col" className="text-center">
                                Action.
                              </th>
                            </tr>
                          </thead>
                          <tbody className="text-center">
                            {currentPost &&
                              Array.isArray(currentPost) &&
                              currentPost.map((user) => (
                                <tr key={user.id}>
                                  <td style={{textTransform:"capitalize"}}>{user.firstname}</td>
                                  <td style={{textTransform:"capitalize"}}>{user.lastname}</td>
                                  <td>{user.userrole}</td>
                                  <td>{user.mobile}</td>
                                  <td>{user.items}</td>
                                  <td>
                                    <Link
                                      to={`/ManageUsers/AddNewUser/${user.id}`}
                                    >
                                      <i class="fas fa-edit"></i>
                                    </Link>
                                  </td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                        <Pagination
                          postPerPage={postPerPage}
                          totalPost={userData?.length}
                          setCurrentPage={handleSetCurrentPage}
                          handleNext={handleNext}
                          handlePrevious={handlePrevious}
                          totalPages={totalPages}
                          currentPage={currentPage}
                        />
                      </div>
                      {/* <NoDataFound /> */}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="m-loader">
                <img
                  src={YELLOWLOADER}
                  style={{ width: "90px", marginBottom: "150px" }}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default UserList;
