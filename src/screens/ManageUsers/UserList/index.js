import React, { useEffect, useState } from "react";
import UserList from "./component/UserList";
import {
  UserRole,
  fetchUserList,
  resetState,
} from "reduxStore/action/userAction";
import { useDispatch, useSelector } from "react-redux";

function Index() {
  const [userData, setUserData] = useState([]);

  const [itemLimit, setItemLimit] = useState(20);
  const [itemOffset, setItemOffset] = useState(0);

  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage, setPostPerPage] = useState(5);
  const [localCurrentPage, setLocalCurrentPage] = useState(1);
  const dispatch = useDispatch();
  const userList = useSelector((state) => state.User);

  const RoleData = useSelector((state) => state.User);

  
  useEffect(() => {
    const item = {
      title: "",
      type: "ROLE",
      limit: 10,
      offset: 0,
    };
    dispatch(UserRole(item));
  }, []);




  const Role = RoleData?.responseUserRole?.data;

  const options = Array.isArray(Role)
    ? Role.map((role) => ({
        value: role?.master_id,
        label: role?.title,
      }))
    : [];

  const [searchValue, setSearchValue] = useState({
    firstname: "",
    role_id: "",
    limit: itemLimit,
    offset: itemOffset,
  });

  // const [totalrecord, setTotalrecord] = useState(10);

  useEffect(() => {
    if (userList && userList?.response?.status === 200) {
      const modiItems = userList?.response?.data;

      setUserData(modiItems);
    } else {
      setUserData("");
    }
  }, [userList]);

  const totalPost = userData?.length;

  const totalPages = Math.ceil(totalPost / postPerPage);
  const handleNext = () => {
    if (currentPage < totalPages) {
      setLocalCurrentPage(currentPage + 1);
      setCurrentPage(currentPage + 1);
    }
  };

  const handlePrevious = () => {
    if (currentPage > 1) {
      setLocalCurrentPage(currentPage - 1);
      setCurrentPage(currentPage - 1);
    }
  };

  const handleSearch = (event) => {
    event.preventDefault(event);
    const updateSearchValue = {
      firstname: searchValue.firstname,
      role_id: searchValue.role_id,
      limit: itemLimit,
      offset: itemOffset,
    };
  
    dispatch(fetchUserList(updateSearchValue));
  };
  useEffect(() => {
    dispatch(fetchUserList(searchValue));
  }, []);

  const lastPostIndex = currentPage * postPerPage;
  const firstPostIndex = lastPostIndex - postPerPage;
  const currentPost = Array.isArray(userData)
    ? userData.slice(firstPostIndex, lastPostIndex)
    : [];

  const handleSetCurrentPage = (page) => {
    setCurrentPage(page);
  };

  return (
    <>
      <UserList
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        handleSearch={handleSearch}
        options={options}
        userData={userData}
        currentPost={currentPost}
        postPerPage={postPerPage}
        totalPost={totalPost}
        handleSetCurrentPage={handleSetCurrentPage}
        handleNext={handleNext}
        handlePrevious={handlePrevious}
        totalPages={totalPages}
        currentPage={currentPage}
      />
    </>
  );
}

export default Index;
