import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { userLogout } from "reduxStore/action/authActions";
import { setDefaultHeader } from "../utils/httpClient";
import strings from "translate/language";
import { CgProfile } from "react-icons/cg";
const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [sidebar, setSidebar] = useState(false);
  const [language, setLanguage] = useState("en");
  console.log("language", language);

  useEffect(() => {
    strings.setLanguage(language);
  }, [language]);

  const LogOut = () => {
    dispatch(userLogout());
    navigate("/login");
  };

  const openclose = () => {
    if (sidebar) {
      setSidebar(false);
      document.querySelector(".page-main-header").classList.remove("open");
      document.querySelector(".page-sidebar").classList.remove("open");
    } else {
      setSidebar(true);
      document.querySelector(".page-main-header").classList.add("open");
      document.querySelector(".page-sidebar").classList.add("open");
    }
  };
  const a= 23;


  return (
    // <div className="page-wrapper">
    <div className="page-main-header">
      <div className="main-header-right row">
        <div className="main-header-left d-lg-none">
          <div className="logo-wrapper">
            <a href="/dashboard">
              <img src="/assets/images/abbylogo-YLWWHT400x160.png" alt="" />
            </a>
          </div>
        </div>
        <div className="mobile-sidebar d-block">
          <div className="media-body text-right switch-sm">
            <label className="switch">
              <a onClick={() => openclose()}>
                <i className="fa fa-bars theme-fa-icon" aria-hidden="true"></i>
              </a>
            </label>
          </div>
        </div>
        <div className="nav-right col d-flex p-0">
          <ul className="nav-menus ">
            <li className="onhover-dropdown">
              <div className="media align-items-center">
                <span>
                  <img
                    className="align-self-center pull-right img-50 rounded-circle"
                    src="/assets/images/user.png"
                  />
                </span>
                <div className="dotted-animation">
                  <span className="animate-circle" />
                  <span className="main-circle" />
                </div>
              </div>
              <ul className="profile-dropdown onhover-show-div p-10">
              <li>
                  <Link to="/Profile">
                  <i class="fa-solid fa-user mr-3" aria-hidden="true"/>{""} 
                    Profile
                    </Link>
                </li>
                <li>
                  <a>
                    {/* <i className="fa fa-sign-out mr-3" aria-hidden="true" />{" "} */}
                    <CgProfile />My Profile
                  </a>
                </li>
                <li>
                <a onClick={() => LogOut()}>
                    <i className="fa fa-sign-out mr-3" aria-hidden="true" />{" "}
                    Logout
                  </a>
                  {/* <span>{strings.copyright}</span> */}
                  {/* <button onClick={() => setLanguage("it")}>Italian</button> */}
                </li>
              
              </ul>
            </li>
          </ul>
          <div className="d-lg-none mobile-toggle pull-right">
            <i data-feather="more-horizontal" />
          </div>
        </div>
      </div>
    </div>
    // </div>
  );
};

export default Header;
