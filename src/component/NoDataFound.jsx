import React from "react";

const NoDataFound = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <img src="./assets/images/nodatafound.png" />
    </div>
  );
};

export default NoDataFound;
