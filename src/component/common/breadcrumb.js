import React, { Fragment } from "react";
import { Home } from "react-feather";
import { Link, useNavigate } from "react-router-dom";
// import Bookmark from './bookmark';

const Breadcrumb = (props) => {
  const breadcrumb = props;
  const navigate = useNavigate();
  const navigatpage = async (navname) => {
    navigate(navname);
  };

  return (
    <Fragment>
      <div className="container-fluid">
        <div className="page-header">
          <div className="row">
            <div className="col">
              <div className="page-header-left">
                <h3>{breadcrumb.title}</h3>
                <ol className="breadcrumb pull-right">
                  <li className="breadcrumb-item">
                    <a>
                      <i
                        className="fa fa-home theme-fa-icon"
                        aria-hidden="true"
                        onClick={() => navigatpage("/dashboard")}
                      ></i>
                    </a>
                  </li>
                  <li className="breadcrumb-item">{breadcrumb.parent}</li>
                  <li className="breadcrumb-item active">{breadcrumb.titleactive}</li>
                </ol>
              </div>
            </div>
            {/* <!-- Bookmark Start--> */}
            {/* <Bookmark /> */}
            {/* <!-- Bookmark Ends--> */}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Breadcrumb;
