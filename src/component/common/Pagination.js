// import React, { useState } from "react";
// import ReactPaginate from "react-paginate";
import { useState } from "react";
import "./pagination.css"
// import Role from "screens/Master/Role/component/Role";

// function Pagination({ itemsPerPage,listData }) {
//   // Here we use item offsets; we could also use page offsets
//   // following the API or data you're working with.
//   const [itemOffset, setItemOffset] = useState(0);

//   // Simulate fetching items from another resources.
//   // (This could be items from props; or items loaded in a local state
//   // from an API endpoint with useEffect and useState)
//   const endOffset = itemOffset + itemsPerPage;
//   console.log(`Loading items from ${itemOffset} to ${endOffset}`);
//   const currentItems = listData?.slice(itemOffset, endOffset);
//   const pageCount = Math.ceil(listData?.length / itemsPerPage);

//   // Invoke when user click to request another page.
//   const handlePageClick = (event) => {
//     const newOffset = (event.selected * itemsPerPage) % listData?.length;
//     console.log(
//       `User requested page number ${event.selected}, which is offset ${newOffset}`
//     );
//     setItemOffset(newOffset);
//   };

//   return (
//     <>
//       {/* <Role currentItems={currentItems} /> */}
//       <ReactPaginate
//         breakLabel="..."
//         nextLabel="next >"
//         onPageChange={handlePageClick}
//         pageRangeDisplayed={5}
//         pageCount={pageCount}
//         previousLabel="< previous"
//         renderOnZeroPageCount={null}
//       />
//     </>
//   );
// }


// export default Pagination;


// import React from 'react'

const Pagination = (props) => {
    const {
        // listData,
        postPerPage,
        totalPost,
        setCurrentPage,
        currentPage,
        handleNext,
        handlePrevious,
        totalPages
    } = props;
    
    let pages = []
    // for (let i = 1; i <= Math.ceil(totalPost / postPerPage); i++) {
    //     pages.push(i)
    // }
    return (
        <div className="pagination">
             <button onClick={handlePrevious} disabled={currentPage === 1} className="previous-next">
                Previous
            </button>
            {/* {
                pages.map((page, index) => {
                    return <button key={index} onClick={() => setCurrentPage(page)}  className="pagination-btn">{page}</button>
                })
            } */}
             <button  className="pagination-btn">{currentPage} / {totalPages}</button>
            <button onClick={handleNext} disabled={currentPage === totalPost} className="previous-next">
                Next
            </button>
            
        </div>
    )
}

export default Pagination