import React, { useState } from "react";
import Multiselect from "multiselect-react-dropdown";

const CustomMultiSelect = ({
  options,
  onSelect,
  onRemove,
  selectionLimit,
  displayValue,
  selectedOptions,
}) => {
  // const [selectedOptions, setSelectedOptions] = useState([]);

  const [inputValue, setInputValue] = useState("");

  const handleCustomButtonClick = () => {
    // Perform your custom action here
    console.log("Custom action when input is not matched.");
  };

  const handleInputValueChanged = (inputValue) => {
    setInputValue(inputValue);
  };

  const filteredOptions = options.filter((option) =>
    option.category_name.toLowerCase().includes(inputValue.toLowerCase())
  );
  return (
    <div className="custom-multi-select">
      <Multiselect
        options={filteredOptions}
        selectedValues={selectedOptions}
        displayValue={displayValue}
        selectionLimit={selectionLimit}
        onSelect={onSelect}
        onRemove={onRemove}
        onSearch={handleInputValueChanged}
        closeOnSelect={true}
        // emptyRecordMsg={
        //   <button
        //     className="btn btn-orange"
        //     // onClick={handleCustomButtonClick}
        //     type="button"
        //     data-toggle="modal"
        //     data-target="#exampleModalLong"
        //     style={{ float: "right" }}
        //   >
        //     Add new category
        //   </button>
        // }
        emptyRecordMsg={'No category found...'}
        
      />
      {/* <!-- Add Category Modal --> */}
      <div
        class="modal fade bd-example-modal-lg"
        id="exampleModalLong"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLongTitle"
        aria-hidden="true"
      >
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">
                Add New category
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div style={{ marginTop: "-82px" }}>
                Add Category
              </div>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomMultiSelect;
