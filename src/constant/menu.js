import {
  Home,
  User,
} from "react-feather";
import base64 from "react-native-base64";

export const MENUITEMS = [
  {
    title: "Dashboard",
    icon: Home,
    path: "/dashboard",
    type: "link",
    badgeType: "primary",
    active: false,
  },
  {
    title: "Manage Customers",
    icon: User,
    type: "sub",
    active: false,
    children: [
      { path: `/customers-list/${base64.encode('1')}`, title: "All Customers List", type: "link" },
      { path: "/customers/add-customer", title: "Add New Customer", type: "link" },
      { path: `/customers/edit-customer/${1}`, title: "Edit Customer", type: "link" },
     
      
    ],
  },  
  {
    title: "Manage Users",
    icon: User,
    type: "sub",
    active: false,
    children: [
      { path: "/Manageusers/Userlist", title: "All User List", type: "link" },
      { path: `/ManageUsers/AddNewUser/''`, title: "Add New User", type: "link" },
     
     
      
    ],
  },
  {
    title: "Manage Master",
    icon: User,
    type: "sub",
    active: false,
    children: [
      { path: "/master/role", title: "Master", type: "link" }
     
     
      
    ],
  },    
];

