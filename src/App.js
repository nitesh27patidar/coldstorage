import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import { jwtTokenGenrate } from "reduxStore/action/authActions";
import { setDefaultHeader } from "utils/httpClient";
import Routers from "./Navigation/Routers";
import '@fortawesome/fontawesome-free/css/all.min.css';

function App() {
  const dispatch = useDispatch();
  const loginSelector = useSelector((state) => state.login);
  const loader = window.document.getElementById("loaderoverlay");
  const [loaderEle, setLoaderEle] = useState(loader);
  
  useEffect(() => {
    if ((loader.style.display) === "block") {
      setTimeout(() => {
        loader.style.display = "none";
      }, 5000);
    }
  }, [loaderEle]);

  useEffect(() => {
    const {response} = loginSelector
    if (response == null) {
      tokenGenrate();
    } else {
      if (response?.status === 200) {
        setDefaultHeader("token", response?.token);
      }
    }
  }, [loginSelector]);

  async function tokenGenrate() {
    dispatch(jwtTokenGenrate());
  }
  return (
    <div>
        <Routers />
    </div>
  );
}

export default App;
