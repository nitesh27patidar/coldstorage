import { combineReducers } from "redux";
import { authStore } from "./authReducers";
import { customerStore } from "./customerReducer";
import { userStore } from "./userReducer";

import { masterStore } from "./masterReducer";

export default combineReducers({
  login: authStore,
 getCustomerList: customerStore,
 User:userStore,

 getMasterList:masterStore,
  
});
