import { ADD_NEW_MASTER, FETCH_MASTER_LIST } from "reduxStore/types";

const initialState = {
    masterList:[],
    response: null,
    loading: true,
    create: false,
    detail: false,
  };

 export function masterStore(state = initialState, action) {
    console.log('action.payload', action.payload)
    switch (action.type) {
      case FETCH_MASTER_LIST:
        return {
          ...state,
          response: action.payload,
        
        };
      case ADD_NEW_MASTER:
        return {
          ...state,
          response: action.payload,
        
        };
        default:
            return state;
        }
      } 