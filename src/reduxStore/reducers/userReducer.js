import { ADD_USERS, CLEAR_USER_DETAILS, EDIT_USER, FETCH_USER_LIST, USER_DETAILS, USER_ROLE } from "../types";

const initialState = {
  response: null,
  loading: true,
  create: false,
  detail: false,
  userDetails:null
};

export function userStore(state = initialState,action) {

  switch (action.type) {
    case ADD_USERS:
      return {
        ...state,
        response: action.payload,
      };
    case FETCH_USER_LIST:
      return {
        ...state,
        response:action.payload,  
        loading: false,
      };
      case USER_DETAILS:
          return{
            ...state,
            userDetails:action.payload,
            loading:false
          };
      case EDIT_USER:
        return{
          ...state,
          response:action.payload,
          loading:false
        };
        case USER_ROLE:
          return{
            ...state,
            responseUserRole:action.payload,
            loading:false
          };
         
         
    default:
      return state;
  }
}
