import {
  TOKEN_GENRATE,
  USER_LOGIN,
  USER_LOGOUT,
  // LOGIN_ERROR,
  // REMOVE_FORGOT_DATA,
  // SAVE_FORGOT_DATA,
  // USER_CHANGEPASSWORD,
  // USER_FORGOT,
  // USER_LOGOUT,
  // USER_OTPVERIFY,
  // USER_UPDATEPASSWORD,
} from "../types";

const initialState = {
  response: null,
  authToken: false,
  loading: true,
  create: false,
  detail: false,
};
// const initialStateForgot = {
//   response: null,
//   otpSend: false,
//   loading: true,
//   reset: false,
// };
// const initialStateChangePassword = {
//   response: null,
//   loading: true,
//   change: false,
// };

export function authStore(state = initialState, action) {
  console.log('action.payload', action.payload)
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        response: action.payload,
        authToken: true,
      };

    case USER_LOGOUT:
      return {
        ...state,
        response: null,
        authToken: false,
      };

    case TOKEN_GENRATE:
      return {
        ...state,
        response: action.payload,
        // authToken: false
      };
    default:
      return state;
  }
}

// export function forgotPassStore(state = initialStateForgot, action) {
//   switch (action.type) {

//     case USER_FORGOT:
//       return {
//         ...state,
//         response: action.payload,
//         reset: false,
//         verifyotp: false,
//         otpSend: true,
//         data:action.data,
//       };
//     case USER_OTPVERIFY:
//       return {
//         ...state,
//         response: action.payload, 
//         otpSend: false,
//         reset: false,
//         verifyotp: true,
//       };
//     case USER_UPDATEPASSWORD:
//       return {
//         ...state,
//         response: action.payload, 
//         reset: true,
//       };
//     case REMOVE_FORGOT_DATA:
//       return {
//         ...state,
//         response: action.payload,
//         authToken: false,
//       };
//       case SAVE_FORGOT_DATA:
//         return {
//           ...state,
//           response: action.payload,
//           authToken: false,
//         };
//         case LOGIN_ERROR:
//           return {
//             ...state,
//             detail: true,
//             create: true,
//             response: action.payload,
//           };
//     default:
//       return state;
//   }
// }
// export function changePassStore(state = initialStateChangePassword, action) {
//   switch (action.type) {

//     case USER_CHANGEPASSWORD:
//       return {
//         ...state,
//         response: action.payload, 
//         change: true,
//       };
//       case REMOVE_FORGOT_DATA:
//       return {
//         ...state,
//         response: action.payload,
//         authToken: false,
//       };
//     default:
//       return state;
//   }
// }
