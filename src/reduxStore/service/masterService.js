import apiEndPoints from "../../utils/apiEndPoints";
import { apiCall } from "../../utils/httpClient";


const getMasterList = async (data) => {
  return await apiCall("post", apiEndPoints.GETMASTER,data);
};
const getNewMaster=async(data)=>{
    return await apiCall("post",apiEndPoints.ADDNEWMASTER,data);
}
const masterService = {
  getMasterList,
  getNewMaster
};

export default masterService;
