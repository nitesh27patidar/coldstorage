import apiEndPoints from "utils/apiEndPoints";
import { apiCall } from "utils/httpClient";



const AddUser = async (data) => {
    console.log('loginDetail',data)
    return await apiCall("POST",apiEndPoints.ADDUSER,data);
  };

 const fetchUserList =async(data)=>{
     return await apiCall("POST",apiEndPoints.USERLIST,data);
 };
  
 const EditUser =async (data)=>{
  return await apiCall("POST",apiEndPoints.EDITUSER,data);
}
const UserDetails= async (data)=>{
  return await apiCall("POST",apiEndPoints.USERDETAILS,data);
}
const UserRole=async (data)=>{
  return await apiCall("POST",apiEndPoints.FORROLE,data);
}
  const userService = {
    AddUser,
    fetchUserList,
    EditUser,
    UserDetails,
    UserRole
  
  };
  
  export default userService;

  