import apiEndPoints from "../../utils/apiEndPoints";
import { apiCall } from "../../utils/httpClient";

const tokenGenrate = async () => {
  const tkn = await apiCall("get", apiEndPoints.JWTTOKEN);
  if (tkn.data.status == 200) {
    return tkn;
  } else {
    return null;
  }
};

const login = async (loginDetail) => {
  console.log('loginDetail', loginDetail)
  return await apiCall("post", apiEndPoints.ADMINLOGIN, loginDetail);
};

// const forgotPassword = async (data) => {
//   return await apiCall("post", apiEndPoints.FORGOTPASSWORD, data);
// };
// const otpVerify = async (data) => {
//   return await apiCall("post", apiEndPoints.OTPVERIFY, data);
// };
// const updatePassword = async (data) => {
//   return await apiCall("post", apiEndPoints.UPDATEPASSWORD, data);
// };
// const ChangePassword = async (data) => {
//   return await apiCall("post", apiEndPoints.CHANGEPASSWORD, data);
// };



const authService = {
  tokenGenrate,
  login,
  // forgotPassword,
  // otpVerify,
  // updatePassword,
  // ChangePassword,
};

export default authService;
