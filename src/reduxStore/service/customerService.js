import apiEndPoints from "../../utils/apiEndPoints";
import { apiCall } from "../../utils/httpClient";


const getCustomerList = async (data) => {
  console.log('loginDetail', data)
  return await apiCall("post", apiEndPoints.CUSTOMERLIST,data);
};

const customerService = {
  getCustomerList,
};

export default customerService;
