import { errorToast, unAuthorizedError } from "utils/httpClient";
import userService from "../service/userService";
import { ADD_USERS, CLEAR_USER_DETAILS, EDIT_USER, FETCH_USER_LIST, RESET_STATE, USER_DETAILS, USER_ROLE } from "../types";

export const AddUser = (data) => async (dispatch) => {
  console.log("Starting addUser API call with data:", data);
  try {
    const res = await userService.AddUser(data);
    console.log("Response:", res);
    if (res.data.status === 200) {
      dispatch({
        type: ADD_USERS,
        payload: res.data,
      });
      return Promise.resolve(res.data);
    } else {
      errorToast(res.data.message);
    }
  } catch (e) {
    const {
      response = {
        data: {
          status: 201,
          message: "Internal error, Please try again!",
        },
      },
    } = e;
    return Promise.reject(response.data.message);
  }
};

export const fetchUserList = (searchValue) => async (dispatch) => {
  try {
    const res = await userService.fetchUserList(searchValue);
    console.log("The res is =====>>>", res);
    dispatch({
      type: FETCH_USER_LIST,
      payload: res.data ,
    });
  } catch (error) {
    return Promise.reject(error.message || "Internal error, Please try again!");
  }
};

export const EditUser = (userInfo) => async (dispatch) => {
 
  try {
    const res = await userService.EditUser(userInfo);
    // console.log("The edituser is =====>>>", res);
    dispatch({
      type: EDIT_USER,
      payload: res.data,
    });
  } catch (error) {
    return error.message;
  }
};



export const UserDetails = (id) => async (dispatch) => { 
  try {
    const res = await userService.UserDetails(id);
    console.log('res: ', res);
    
    dispatch({
      type: USER_DETAILS,
      payload: res.data,
    });
  } catch (error) {
    console.log('error.message: ', error.message);
    return error.message;
  }
};

export const UserRole = (item) => async (dispatch) => { 
  try {
    const res = await userService.UserRole(item);
   
    dispatch({
      type: USER_ROLE,
      payload: res.data,
    });
  } catch (error) {
    console.log('error.message: ', error.message);
    return error.message;
  }
};



export const resetState = () => ({
  type: RESET_STATE
});
