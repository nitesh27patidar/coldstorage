import { unAuthorizedError } from "utils/httpClient";
import { ADD_NEW_MASTER, CREATE_MASTER, FETCH_MASTER_LIST} from "../types"
import masterService from "reduxStore/service/masterService";



export const getMasterList = (item) => async (dispatch) => {
    try {
       const res = await masterService.getMasterList(item);
       console.log('res111', res);
       if (res.data.status === 200){
      dispatch({
        type:FETCH_MASTER_LIST,
        payload:res.data,
      }); 
      }
    } catch (e) {
        const {
          response = {
            data: {
              status: 201,
              message: "Internal error, Please try again!",
            },
          },
        } = e;
        unAuthorizedError(response.data);
      }
  }; 
  
export const getNewMaster = (item) => async (dispatch) => {
    try {
       const res = await masterService.getNewMaster(item);
       console.log('res111', res);
       if (res.data.status === 200){
      dispatch({
        type:ADD_NEW_MASTER,
        payload:res.data,
      }); 
      }
    } catch (e) {
        const {
          response = {
            data: {
              status: 201,
              message: "Internal error, Please try again!",
            },
          },
        } = e;
        unAuthorizedError(response.data);
      }
  }; 
  