import { unAuthorizedError } from "utils/httpClient";
import customerService from "../service/customerService"
import {CUSTOMER_LIST} from "../types"



export const getCustomerList = (item) => async (dispatch) => {
  console.log('hii');
    try {
       const res = await customerService.getCustomerList(item);
       console.log('res', res);
       if (res.data.status === 200){
      dispatch({
        type:CUSTOMER_LIST,
        payload:res.data,
      }); 
      }
    } catch (e) {
        const {
          response = {
            data: {
              status: 201,
              message: "Internal error, Please try again!",
            },
          },
        } = e;
        unAuthorizedError(response.data);
      }
  }; 
  