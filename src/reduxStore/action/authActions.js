import { LOGIN_ERROR, PERMISSION_MODULES, TOKEN_GENRATE, USER_LOGIN, USER_LOGOUT } from "../types";
import authService from "../service/authService";
import {
  errorToast,
  unAuthorizedError,
  successToast,
} from "../../utils/httpClient";
import { useNavigate } from "react-router-dom";
export const jwtTokenGenrate = () => async (dispatch) => {
  try {
    const res = await authService.tokenGenrate();
    if (res.data.status === 200) {
      dispatch({
        type: TOKEN_GENRATE,
        payload: res.data,
      });
    } 
  } catch (e) {
    const {
      response = {
        data: {
          status: 501,
          message: "Internal error, Please try again!",
        },
      },
    } = e;
    unAuthorizedError(response.data);
  }
};
export const userLogin = (item) => async (dispatch) => {
  try {
    const res = await authService.login(item);
    console.log('res', res)
    if (res.data.status === 200) {
      dispatch({
        type: USER_LOGIN,
        payload: res.data,
      });

    } else {
      errorToast(res.data.message);
    }
  } catch (e) {
    const {
      response = {
        data: {
          status: 501,
          message: "Internal error, Please try again!",
        },
      },
    } = e;
    if (response.data.status === 401 || response.data.status === 400) {
      dispatch(jwtTokenGenrate());
      unAuthorizedError(response.data);
    }
  }
};

export const userLogout = () => async (dispatch) => {
  try {
    await window.localStorage.removeItem("persistantState");
    await window.localStorage.clear();
    dispatch({
      type: USER_LOGOUT,
      payload: null,
    });
  } catch (e) {
    const { response = "Logout failed" } = e;
    dispatch({
      type: LOGIN_ERROR,
      payload: response.data,
    });
  }
};

// export const forgotPassword = (item) => async (dispatch) => {
//   try {
//     const res = await authService.forgotPassword(item);
//     if (res.data.status === 200) {
//       dispatch({
//         type: USER_FORGOT,
//         payload: res.data,
//         data: item,
//       });
//       successToast(res.data.message);
//     } else {
//       errorToast(res.data.message);
//     }
//   } catch (e) {
//     const { response = strings.initialError } = e;
//     errorToast(response?.data?.message);
//   }
// };

// export const forgotRemoveData = (item) => async (dispatch) => {
//   try {
//     // if (res.data.status === 200) {
//     dispatch({
//       type: REMOVE_FORGOT_DATA,
//       payload: null,
//     });
//     // } else {
//     // errorToast(res.data.message)
//     // }
//   } catch (e) {
//     const { response = strings.initialError } = e;
//     // dispatch({
//     //     type: LOGIN_ERROR,
//     //     payload: response.data,
//     // })
//     unAuthorizedError(response.data);
//   }
// };

// export const otpVerify = (item) => async (dispatch) => {
//   try {
//     const res = await authService.otpVerify(item);
//     if (res.data.status === 200) {
//       dispatch({
//         type: USER_OTPVERIFY,
//         payload: res.data,
//       });
//       successToast(res.data.message);
//     } else {
//       errorToast(res.data.message);
//     }
//   } catch (e) {
//     const { response = strings.initialError } = e;

//     errorToast(response?.data?.message);
//   }
// };

// export const updatePassword = (item) => async (dispatch) => {
//   try {
//     const res = await authService.updatePassword(item);
//     if (res.data.status === 200) {
//       dispatch({
//         type: USER_UPDATEPASSWORD,
//         payload: res.data,
//       });
//       successToast(res.data.message);
//     } else {
//       errorToast(res.data.message);
//     }
//   } catch (e) {
//     const { response = strings.initialError } = e;
//     unAuthorizedError(response.data);
//   }
// };

// export const ChangePassword = (item) => async (dispatch) => {
//   try {
//     const res = await authService.ChangePassword(item);
//     if (res.data.status === 200) {
//       dispatch({
//         type: USER_CHANGEPASSWORD,
//         payload: res.data,
//       });
//       successToast(res.data.message);
//     } else {
//       errorToast(res.data.message);
//     }
//   } catch (e) {
//     const { response = strings.initialError } = e;
//     unAuthorizedError(response.data);
//   }
// };
