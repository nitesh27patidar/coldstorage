import React, { Fragment } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import Header from "../component/Header";
import SideBar from "../component/SideNav";
import Footer from "../component/Footer";
import Dashboard from "../screens/Dashboard";
import Login from "../screens/Auth/Login";
import {useSelector } from "react-redux";
import AddCustomer from "screens/ManageCustomers/Index";
import AllCustomerList from "screens/ManageCustomers/AllCustomerList";
import ViewCustomer from "screens/ManageCustomers/ViewCustomer"
import AddItems from "screens/ManageCustomers/AddItems"
import AddNewUser from "screens/ManageUsers/AddNewUser";
import UserList from "screens/ManageUsers/UserList/index";
import Profile from "screens/Profile";

import Role from "screens/Master/Role";
// import '../../public/assets/css/style.css'



function Routers() {
  const isAuth = useSelector((state) => {
    const {response ,authToken} = state.login
    if (response && authToken) {
      return true
    } else {
      return false
    }
  });

  return (
    <div className="App">
      <Fragment>
        <Routes>
          <Route path={`/`} element={<Login />} />
          <Route path={`/login`} element={<Login />} />
          <Route
            path={`/*`}
            element={isAuth ? <AuthRoute /> : <Navigate to="/login" />}
          />
          
        </Routes>
      </Fragment>
    </div>
  );
}

export default Routers;

const AuthRoute = () => {
  return (
    <div className="page-wrapper">
      <Header />
      <div className="page-body-wrapper">
        <SideBar />
        <Routes>
          <Route path={`/dashboard`} element={<Dashboard />} />
          <Route
            path={`/customers-list/:page_no`}
            element={<AllCustomerList />}
          />
          <Route path={`/customers/add-customer`} element={<AddCustomer />} />
          <Route path={`/customers/edit-customer/:id`} element={<AddCustomer />} />
          <Route path={`/customers/view-customer/:id`} element={<ViewCustomer />} />
          <Route path={`/customers/add-items/:id`} element={<AddItems/>}/>
          <Route path={"/ManageUsers/AddNewUser/:id"} element={<AddNewUser/>}/>

          <Route path={"/Manageusers/Userlist/"} element={<UserList/>}/>
          <Route path={"/Profile"} element={<Profile/>}/>
         

          <Route path="/master/role" element={<Role/>}/>
            
            
        </Routes>
        <Footer />
      </div>
    </div>
  );
};
