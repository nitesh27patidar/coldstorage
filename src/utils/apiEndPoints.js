export default {
  ADMINLOGIN: "authuser/login",
  JWTTOKEN: "jwtToken/tokenGenerate",
  ADDCUSTOMER: "customer/addcustomer",
  CUSTOMERLIST:"customer/customerList",
  ADDCUSTOMER:"customer/addItems",
  ADDUSER:"user/addnewuser",
  USERLIST:"user/getuserlist",
  USERDETAILS:"user/userdetails",
  EDITUSER:"user/userupdate",
  FORROLE:"master/getmasterlist",

  GETMASTER:"master/getmasterlist",
  ADDNEWMASTER:"master/addnewmaster"
};
