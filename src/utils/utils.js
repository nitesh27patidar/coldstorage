export const clearUndefinedObject = (obj) => {
  Object.keys(obj).forEach((key) => {
    if (obj[key] === undefined) {
      delete obj[key];
    } else if (obj[key] === "undefined") {
      delete obj[key];
    }
  });
  return obj;
};

export function handleDetailResponse(result) {
  let data = "";
  if (Array.isArray(result)) {
    data = result[0];
  } else {
    data = result;
  }
  return data ? clearUndefinedObject(data) : {};
}

export function handleListResponse(result) {
  let data;
  if (Array.isArray(result)) {
    data = result.map((item) => clearUndefinedObject(item));
  } else {
    data = [];
  }
  return data;
}
